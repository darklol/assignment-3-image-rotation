#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdlib.h>

enum open_status {
    OPEN_SUCCESS,
    OPEN_OTHER_ERROR,
    OPEN_FILE_PERM_DENIED,
    OPEN_FILE_NOT_EXIST
};

enum open_status file_open(FILE** file, const char* path, const char* user_rights);

void emergency_exit(const char* message);

#endif


