#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "util.h"


int main(int argc, char** argv) {
    if (argc < 3)
        emergency_exit("Неверный формат аргументов");

    FILE *file_in;
    enum open_status open_status_in = file_open(&file_in, argv[1], "rb");
    switch (open_status_in) {
        case OPEN_FILE_NOT_EXIST:
            emergency_exit("Файл ввода не найден");
        case OPEN_FILE_PERM_DENIED:
            emergency_exit("Нет разрешения для доступа к файлу ввода");
        case OPEN_OTHER_ERROR:
            emergency_exit("Неизвестная ошибка чтения файла ввода");
        case OPEN_SUCCESS:
            break;
    }

    FILE* file_out;
    enum open_status open_status_out = file_open(&file_out, argv[2], "wb");
    switch (open_status_out) {
        case OPEN_FILE_NOT_EXIST:
            fprintf(stderr, "Файл вывода не найден");
        case OPEN_FILE_PERM_DENIED:
            fprintf(stderr, "Нет разрешения для доступа к файлу вывода");
            break;
        case OPEN_OTHER_ERROR:
            fprintf(stderr, "Неизвестная ошибка чтения файла вывода");
            break;
        case OPEN_SUCCESS:
            break;
    }

    struct image input_img = {0};
    enum read_status Read_status = from_bmp(file_in, &input_img);
    switch (Read_status) {
        case READ_INVALID_BITS:
            fprintf(stderr, "Файл повреждён");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Ошибка чтения заголовка");
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Неверная подпись");
            break;
        case READ_OK:
            break;
    }

    struct image rotated_img = rotate(input_img);
    enum write_status Write_status = to_bmp(file_out, &rotated_img);
    switch (Write_status) {
        case WRITE_ERROR:
            fprintf(stderr, "Ошибка записи");
        case WRITE_OK:
            break;
    }

    fclose(file_in);
    fclose(file_out);
    destroy_image(&input_img);
    destroy_image(&rotated_img);

    return 0;
}
