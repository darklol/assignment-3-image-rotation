#include "bmp.h"
#include <stdlib.h>

static const uint16_t BMP_FILE_SIGNATURE = 0x4d42;
static const uint32_t HEADER_INFO_SIZE = 40;
static const uint16_t BITS_PER_PIXEL = 24;

const uint8_t PADDING_BYTES[4] = {0};

size_t get_padding(size_t width) {
    return width % 4;
}

size_t get_size(const struct image* img) {
    size_t width = img->width;
    size_t height = img->height;
    return (width * sizeof(struct pixel) + get_padding(width)) * height;
}

struct bmp_header get_header(const struct image* img) {
    return (struct bmp_header) {
            .bfType = BMP_FILE_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * img->width * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_INFO_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = 0,
            .biSizeImage = get_size(img),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

    };
}


enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = get_header(img);
    if (!fread(&header, sizeof(struct bmp_header), 1, in))
        return READ_INVALID_HEADER;

    size_t padding = get_padding(header.biWidth);
    *img = create_image(header.biWidth, header.biHeight);

    for (size_t i = 0; i < img->height; i++) {
        if (!fread(img->data + i * img->width, sizeof(struct pixel), img->width,
                   in))
            return READ_INVALID_BITS;
        if (fseek(in, (long)padding, SEEK_CUR))
            return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    size_t padding = get_padding(img->width);

    struct bmp_header header = get_header(img);
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width, sizeof(struct pixel),
                    img->width, out))
            return WRITE_ERROR;
        if (!fwrite(&PADDING_BYTES, 1, padding, out))
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
