#include "image.h"
#include <stdlib.h>

struct image create_image(const size_t width, const size_t height) {
    return ((struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))});

}


void destroy_image(struct image* image) {
    free(image->data);
}
