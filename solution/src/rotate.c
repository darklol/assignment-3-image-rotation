#include "rotate.h"

struct image rotate(struct image const source) {
    struct image transformed = create_image(source.height, source.width);
    for (size_t w = 0; w < source.width; w++) {
        for (size_t h = 0; h < source.height; h++) {
            transformed.data[(w + 1) * source.height - h - 1] =
                source.data[h * source.width + w];
        }
    }
    return transformed;
}
